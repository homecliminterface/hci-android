package net.tardieu.roland.hciandroid.data.local

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import net.tardieu.roland.hciandroid.data.Valve

@Database(entities = [Valve::class], version = 1, exportSchema = false)
abstract class HCIAndroidDatabase : RoomDatabase() {
    abstract fun valveDao(): ValveDao

    companion object {
        @Volatile
        private var Instance: HCIAndroidDatabase? = null

        fun getDatabase(context: Context): HCIAndroidDatabase {
            return Instance ?: synchronized(this) {
                Room.databaseBuilder(
                    context,
                    HCIAndroidDatabase::class.java,
                    "hci_android_database"
                )
                    .build()
                    .also { Instance = it }
            }
        }
    }
}