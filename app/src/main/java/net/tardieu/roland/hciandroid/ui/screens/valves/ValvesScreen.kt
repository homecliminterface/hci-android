package net.tardieu.roland.hciandroid.ui.screens.valves

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.foundation.layout.wrapContentWidth
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Star
import androidx.compose.material3.Button
import androidx.compose.material3.Card
import androidx.compose.material3.Icon
import androidx.compose.material3.LinearProgressIndicator
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import com.google.accompanist.swiperefresh.SwipeRefresh
import com.google.accompanist.swiperefresh.rememberSwipeRefreshState
import kotlinx.coroutines.flow.map
import net.tardieu.roland.hciandroid.R
import net.tardieu.roland.hciandroid.data.UpdateStatus
import net.tardieu.roland.hciandroid.ui.AppViewModelProvider

@Composable
fun ValvesScreen(
    viewModel: ValvesViewModel = viewModel(factory = AppViewModelProvider.Factory),
) {
    val valvesState by viewModel.valvesState.collectAsState()
    val lastUpdateDateState by viewModel.lastUpdateDateState.collectAsState()
    val isRefreshing = viewModel.updateStatusState
        .map { updateStatus ->
            updateStatus is UpdateStatus.InProgress
        }
        .collectAsState(initial = false)
        .value
    val swipeRefreshState = rememberSwipeRefreshState(isRefreshing = isRefreshing)

    SwipeRefresh(
        state = swipeRefreshState,
        onRefresh = viewModel::refresh,
        modifier = Modifier
            .fillMaxWidth()
    ) {
        Column {
            Text(text = lastUpdateDateState.toString())
            LazyColumn(
                modifier = Modifier
                    .fillMaxSize()
            ) {
                items(valvesState.valves) {
                    ValveCard(
                        valve = it,
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(top = 8.dp)
                    )
                }
            }
        }
    }
}

@Composable
fun ValveCard(
    valve: ValveForUi,
    modifier: Modifier = Modifier
) {
    val heatingDemandColor = if (valve.heatRequired) Color.Red else Color.Cyan
    val maxTemperatureDelta = 1f
    val measuredTemperatureColor =
        if (valve.measuredTemperature < valve.wantedTemperature - maxTemperatureDelta)
            Color.Blue
        else if (valve.measuredTemperature <= valve.wantedTemperature + maxTemperatureDelta)
            Color.Green
        else
            Color.Red

    Card(
        modifier = modifier,
    ) {
        Column(
            modifier = Modifier
                .padding(all = 16.dp)
                .fillMaxWidth()
        ) {
            Column {
                Text(
                    text = valve.friendlyName,
                    style = MaterialTheme.typography.titleLarge,
                    modifier = Modifier
                        .fillMaxWidth()
                        .wrapContentWidth(Alignment.CenterHorizontally)
                )
                Text(
                    text = valve.scheduleName ?: stringResource(R.string.valve_no_schedule),
                    style = MaterialTheme.typography.bodyMedium
                )
            }
            Row(
                modifier = Modifier
                    .fillMaxWidth()
                    .wrapContentWidth(Alignment.CenterHorizontally)
            ) {
                Spacer(modifier = Modifier.weight(1f))
                Row(
                    verticalAlignment = Alignment.CenterVertically
                ) {
                    Icon(
                        painter = painterResource(id = R.drawable.target_icon),
                        contentDescription = stringResource(id = R.string.valve_wanted_temperature),
                    )
                    Spacer(modifier = Modifier.width(4.dp))
                    Text(
                        text = stringResource(
                            id = R.string.valve_wanted_temperature_template,
                            valve.wantedTemperature
                        ),
                        style = MaterialTheme.typography.displaySmall,
                    )
                }
                Spacer(modifier = Modifier.weight(1f))
                Row(
                    verticalAlignment = Alignment.CenterVertically,
                ) {
                    Icon(
                        painter = painterResource(id = R.drawable.temperature_icon),
                        contentDescription = stringResource(R.string.valve_measured_temperature)
                    )
                    Text(
                        text = stringResource(
                            id = R.string.valve_measured_temperature_template,
                            valve.measuredTemperature
                        ),
                        style = MaterialTheme.typography.displaySmall,
                        color = measuredTemperatureColor,
                    )
                }

                Spacer(modifier = Modifier.weight(1f))
            }
            Row(
                verticalAlignment = Alignment.CenterVertically,
            ) {
                Text(
                    text = stringResource(
                        id = R.string.valve_heating_demand,
                        valve.heatingDemand
                    ),
                    color = heatingDemandColor,
                )
                Spacer(modifier = Modifier.width(4.dp))
                LinearProgressIndicator(
                    progress = (valve.heatingDemand.toFloat()) / 100,
                    modifier = Modifier
                        .fillMaxWidth(),
                    color = heatingDemandColor,
                )
            }

        }
    }
}