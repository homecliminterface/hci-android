package net.tardieu.roland.hciandroid

import android.app.Application
import net.tardieu.roland.hciandroid.data.AppContainer
import net.tardieu.roland.hciandroid.data.AppDataContainer

class HCIAndroidApplication : Application() {

    /**
     * AppContainer instance used by the rest of classes to obtain dependencies
     */
    lateinit var container: AppContainer

    override fun onCreate() {
        super.onCreate()
        container = AppDataContainer(this) // For normal use
    }
}