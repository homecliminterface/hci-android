package net.tardieu.roland.hciandroid.data

import android.adservices.adid.AdId
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "valves")
data class Valve(
    @PrimaryKey(autoGenerate = false)
    val friendlyName: String,
    val heatRequired: Boolean,
    val measuredTemperature: Float,
    val wantedTemperature: Float,
    val heatingDemand: Int,
    val scheduleId: Int,
)
