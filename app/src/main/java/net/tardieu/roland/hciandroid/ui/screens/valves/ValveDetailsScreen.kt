package net.tardieu.roland.hciandroid.ui.screens.valves

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.Card
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.lifecycle.viewmodel.compose.viewModel
import net.tardieu.roland.hciandroid.ui.AppViewModelProvider

@Composable
fun ValveDetailsScreen(
    viewModel: ValvesViewModel = viewModel(factory = AppViewModelProvider.Factory)
) {
    val valveDetailsState by viewModel.valveDetailsState.collectAsState()

    ValveDetailsCard(
        valve = valveDetailsState.valve,
        aggressiveTemperatureUpdate = valveDetailsState.aggressiveTemperatureUpdate,
        modifier = Modifier
            .fillMaxSize()
    )
}

@Composable
fun ValveDetailsCard(
    valve: ValveForUi,
    aggressiveTemperatureUpdate: Boolean,
    modifier: Modifier = Modifier
) {
    Card(
        modifier = modifier
    ) {
        Column(
            modifier = Modifier
                .fillMaxSize()
        ) {

        }
    }
}