package net.tardieu.roland.hciandroid.ui.screens.settings

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.last
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import net.tardieu.roland.hciandroid.data.UserPreferencesRepository

class SettingsViewModel(
    private val userPreferencesRepository: UserPreferencesRepository
) : ViewModel() {

    private val _settingsState = MutableStateFlow(SettingsState())
    val settingsState: StateFlow<SettingsState> = _settingsState.asStateFlow()

    init {
        updateSettingsStateFromUserPreferencesRepository()
    }


    fun updateUsername(
        username: String,
    ) {
        _settingsState.update { currentState ->
            currentState.copy(username = username)
        }
    }

    fun updatePassword(
        password: String,
    ) {
        _settingsState.update { currentState ->
            currentState.copy(password = password)
        }
    }

    fun updateServerAddress(
        serverAddress: String,
    ) {
        _settingsState.update { currentState ->
            currentState.copy(serverAddress = serverAddress)
        }
    }

    fun onPasswordVisibilityClicked() {
        _settingsState.update { currentState ->
            currentState.copy(showPassword = currentState.showPassword.not())
        }
    }

    fun onSaveServerAddressButtonClicked() {
        val serverAddress = settingsState.value.serverAddress
        viewModelScope.launch {
            userPreferencesRepository.saveServerAddress(serverAddress)
        }
    }

    fun onSaveCredentialsButtonClicked() {
        val username = settingsState.value.username
        val password = settingsState.value.password
        viewModelScope.launch {
            userPreferencesRepository.saveUsername(username)
            userPreferencesRepository.savePassword(password)
        }
    }

    private fun updateSettingsStateFromUserPreferencesRepository() {
        viewModelScope.launch(Dispatchers.IO) {
            _settingsState.update { currentState ->
                currentState.copy(
                    username = userPreferencesRepository.username.first(),
                    password = userPreferencesRepository.password.first(),
                    serverAddress = userPreferencesRepository.serverAddress.first()
                )
            }
        }
    }

}