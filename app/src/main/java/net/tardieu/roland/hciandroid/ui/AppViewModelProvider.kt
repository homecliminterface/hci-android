package net.tardieu.roland.hciandroid.ui

import android.app.Application
import android.util.Log
import androidx.lifecycle.ViewModelProvider.AndroidViewModelFactory
import androidx.lifecycle.viewmodel.CreationExtras
import androidx.lifecycle.viewmodel.initializer
import androidx.lifecycle.viewmodel.viewModelFactory
import net.tardieu.roland.hciandroid.HCIAndroidApplication
import net.tardieu.roland.hciandroid.ui.screens.settings.SettingsViewModel
import net.tardieu.roland.hciandroid.ui.screens.valves.ValvesViewModel

private const val TAG = "INITIALIZER"

/**
 * Provides Factory to create instance of ViewModel for the entire Inventory app
 */
object AppViewModelProvider {
    val Factory = viewModelFactory {
        initializer {
            Log.d(TAG, "Initializer of ValvesViewModel called")
            ValvesViewModel(
                valveRepository = hciAndroidApplication().container.valveRepository,
                accessAndRefreshTokenRepository = hciAndroidApplication().container.accessAndRefreshTokenRepository,
                accessTokenRepository = hciAndroidApplication().container.accessTokenRepository,
                userPreferencesRepository = hciAndroidApplication().container.userPreferencesRepository
            )
        }
        initializer {
            Log.d(TAG, "Initializer of SettingsViewModel called")
            SettingsViewModel(hciAndroidApplication().container.userPreferencesRepository)
        }
    }
}


/**
 * Extension function to queries for [Application] object and returns an instance of
 * [HCIAndroidApplication].
 */
fun CreationExtras.hciAndroidApplication(): HCIAndroidApplication =
    (this[AndroidViewModelFactory.APPLICATION_KEY] as HCIAndroidApplication)
