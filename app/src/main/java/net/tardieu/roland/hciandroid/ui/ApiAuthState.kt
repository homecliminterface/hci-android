package net.tardieu.roland.hciandroid.ui

data class ApiAuthState(
    val username: String = "",
    val password: String = "",
    val accessToken: String = "",
    val refreshToken: String = "",
)