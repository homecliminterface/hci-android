package net.tardieu.roland.hciandroid.ui.screens.settings

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowDropDown
import androidx.compose.material.icons.filled.Search
import androidx.compose.material3.Button
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.text.input.VisualTransformation
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import net.tardieu.roland.hciandroid.R
import net.tardieu.roland.hciandroid.ui.AppViewModelProvider

@Composable
fun SettingsScreen(
    viewModel: SettingsViewModel = viewModel(factory = AppViewModelProvider.Factory)
) {
    val state by viewModel.settingsState.collectAsState()
    val focusManager = LocalFocusManager.current

    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(all = 4.dp)
    ) {
        Text(
            text = stringResource(id = R.string.settings_server_address),
            modifier = Modifier
                .padding(bottom = 4.dp),
            style = MaterialTheme.typography.headlineMedium
        )
        TextField(
            value = state.serverAddress,
            onValueChange = { viewModel.updateServerAddress(it) },
            singleLine = true,
            label = {
                Text(text = stringResource(id = R.string.settings_server_address))
            },
            placeholder = {
                Text(text = "https://")
            },
            keyboardOptions = KeyboardOptions.Default.copy(
                autoCorrect = false,
                keyboardType = KeyboardType.Uri,
                imeAction = ImeAction.Done
            ),
            keyboardActions = KeyboardActions(
                onDone = {
                    focusManager.clearFocus()
                }
            ),
            modifier = Modifier
                .fillMaxWidth()
        )
        Button(
            onClick = viewModel::onSaveServerAddressButtonClicked,
            modifier = Modifier
                .fillMaxWidth()
                .padding(bottom = 8.dp)
        ) {
            Text(text = stringResource(id = R.string.settings_save))
        }
        
        Text(
            text = stringResource(id = R.string.settings_authentication_information),
            modifier = Modifier
                .padding(bottom = 4.dp),
            style = MaterialTheme.typography.headlineMedium
        )
        TextField(
            value = state.username,
            onValueChange = { viewModel.updateUsername(it) },
            singleLine = true,
            label = {
                Text(text = stringResource(id = R.string.settings_username))
            },
            keyboardOptions = KeyboardOptions.Default.copy(
                autoCorrect = false,
                keyboardType = KeyboardType.Text,
                imeAction = ImeAction.Next
            ),
            modifier = Modifier
                .fillMaxWidth()
                .padding(bottom = 4.dp)
        )
        TextField(
            value = state.password,
            onValueChange = { viewModel.updatePassword(it) },
            singleLine = true,
            label = {
                Text(text = stringResource(id = R.string.settings_password))
            },
            keyboardOptions = KeyboardOptions.Default.copy(
                autoCorrect = false,
                keyboardType = KeyboardType.Password,
                imeAction = ImeAction.Done,
            ),
            keyboardActions = KeyboardActions(
                onDone = {
                    focusManager.clearFocus()
                }
            ),
            visualTransformation = if (state.showPassword) VisualTransformation.None
                    else PasswordVisualTransformation(),
            trailingIcon = {
                val (icon, descRes) = if (!state.showPassword)
                    Pair(Icons.Default.Search, R.string.settings_show_password)
                    else Pair(Icons.Default.ArrowDropDown, R.string.settings_hide_password)
                IconButton(onClick = viewModel::onPasswordVisibilityClicked) {
                    Icon(icon, stringResource(descRes))
                }
            },
            modifier = Modifier
                .fillMaxWidth()
        )
        Button(
            onClick = viewModel::onSaveCredentialsButtonClicked,
            modifier = Modifier
                .fillMaxWidth()
                .padding(bottom = 8.dp)
        ) {
            Text(text = stringResource(id = R.string.settings_save))
        }
    }

}

@Preview(showBackground = true)
@Composable
fun SettingsScreenPreview() {
    SettingsScreen()
}