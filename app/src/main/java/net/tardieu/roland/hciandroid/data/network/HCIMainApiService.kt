package net.tardieu.roland.hciandroid.data.network

import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.POST

interface HCIMainApiService {
    @GET("heater_valve")
    suspend fun getValves(
        @Header(value = "Authorization") bearerToken: String,
    ): Response<List<ApiValve>>


}