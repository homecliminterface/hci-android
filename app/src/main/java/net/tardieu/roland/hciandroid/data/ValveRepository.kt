package net.tardieu.roland.hciandroid.data

import android.util.Log
import androidx.compose.runtime.MutableState
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.runBlocking
import net.tardieu.roland.hciandroid.data.local.ValveDao
import net.tardieu.roland.hciandroid.data.network.ApiResponse
import net.tardieu.roland.hciandroid.data.network.ApiValve
import net.tardieu.roland.hciandroid.data.network.Credentials
import net.tardieu.roland.hciandroid.data.network.HCIAuthApiService
import net.tardieu.roland.hciandroid.data.network.HCIMainApiService
import net.tardieu.roland.hciandroid.data.network.RefreshToken
import net.tardieu.roland.hciandroid.data.network.apiRequestFlow
import net.tardieu.roland.hciandroid.data.network.toValve
import retrofit2.Response
import java.io.IOException
import java.net.SocketTimeoutException
import java.net.UnknownHostException
import java.time.LocalDateTime
import java.util.Calendar
import java.util.Date


/**
 * Repository thate provides insert, update, delete, and retrieve of [Valve] from a given data source
 */
interface ValveRepository {
    val allValvesStream: Flow<RepositoryResponse<List<Valve>>>
    val updateStatus: StateFlow<UpdateStatus>

    fun getValveStream(friendlyName: String): Flow<Valve?>
    suspend fun insertValve(valve: Valve)
    suspend fun deleteValve(valve: Valve)
    suspend fun updateValve(valve: Valve)

    fun setAccessToken(accessToken: String)
    suspend fun updateValvesListFromApi()

}

sealed interface UpdateStatus {
    data class Done(val date: Date) : UpdateStatus
    data object InProgress : UpdateStatus
    data object AccessTokenInvalid : UpdateStatus
    data object AccessTokenNotPresent : UpdateStatus
    data object AccessTokenRefreshed : UpdateStatus
    data object Undefined: UpdateStatus
    data object ServerNotAccessible: UpdateStatus
}

/**
 * Implementation of [ValveRepository] which manage data from local and network datasources
 */
class ValveRepositoryImplementation(
    private val valveDao: ValveDao,
    private val hciMainApiService: HCIMainApiService,
) : ValveRepository {
    companion object {
        private const val TAG = "ValveRepository"
    }

    private var accessToken: String? = null

    override val allValvesStream = valveDao.getAllValves().map { valvesList ->
        RepositoryResponse.Success(valvesList)
    }

    private var _updateStatus: MutableStateFlow<UpdateStatus> = MutableStateFlow(UpdateStatus.Undefined)
    override val updateStatus = _updateStatus.asStateFlow()

    override fun getValveStream(friendlyName: String): Flow<Valve?> = valveDao.getValve(friendlyName)

    override suspend fun insertValve(valve: Valve) = valveDao.insert(valve)

    override suspend fun deleteValve(valve: Valve) = valveDao.delete(valve)

    override suspend fun updateValve(valve: Valve) = valveDao.update(valve)

    override fun setAccessToken(accessToken: String) {
        this.accessToken = accessToken
        _updateStatus.update { UpdateStatus.AccessTokenRefreshed }
    }

    override suspend fun updateValvesListFromApi() {
        if (accessToken == null) {
            _updateStatus.update { UpdateStatus.AccessTokenNotPresent }
        } else {
            _updateStatus.update { UpdateStatus.InProgress }
            var apiServiceResponse: Response<List<ApiValve>>? = null
            try {
                val apiServiceResponse = hciMainApiService.getValves("Bearer $accessToken")
                if (apiServiceResponse.isSuccessful) {
                    Log.d(TAG, "apiValvesList obtained")
                    val valvesList = apiServiceResponse.body()?.map { apiValve -> apiValve.toValve() }
                        ?: listOf()
                    for (valve in valvesList) {
                        val databaseValve = valveDao.getValve(
                            friendlyName = valve.friendlyName
                        ).first()
                        if (databaseValve == null) {
                            Log.d(TAG, "Inserting valve: $valve")
                            valveDao.insert(valve)
                        } else if (databaseValve != valve) {
                            Log.d(TAG, "Updating valve: $valve")
                            valveDao.update(valve)
                        }


                    }
                    Log.d(TAG, "Update finished")
                    _updateStatus.update { UpdateStatus.Done(Calendar.getInstance().time) }
                } else {
                    Log.e(TAG, "apiValvesList not obtained")
                    Log.e(TAG, apiServiceResponse.errorBody().toString())
                    if (apiServiceResponse.code() == 401) {
                        _updateStatus.update { UpdateStatus.AccessTokenInvalid }
                    } else {
                        _updateStatus.update { UpdateStatus.ServerNotAccessible }
                    }
                }
            } catch (e: SocketTimeoutException) {
                Log.e(TAG, e.toString())
                _updateStatus.update { UpdateStatus.ServerNotAccessible }
            } catch (e: UnknownHostException) {
                Log.e(TAG, e.toString())
                _updateStatus.update { UpdateStatus.ServerNotAccessible }
            }

        }
    }

}
