package net.tardieu.roland.hciandroid.data.local

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Update
import kotlinx.coroutines.flow.Flow
import net.tardieu.roland.hciandroid.data.Valve

@Dao
interface ValveDao {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insert(valve: Valve)

    @Update
    suspend fun update(valve: Valve)

    @Delete
    suspend fun delete(valve: Valve)

    @Query("SELECT * FROM valves WHERE friendlyName = :friendlyName")
    fun getValve(friendlyName: String): Flow<Valve?>

    @Query("SELECT * FROM valves ORDER BY friendlyName")
    fun getAllValves(): Flow<List<Valve>>
}