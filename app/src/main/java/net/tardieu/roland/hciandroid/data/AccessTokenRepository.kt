package net.tardieu.roland.hciandroid.data

import android.util.Log
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import net.tardieu.roland.hciandroid.data.network.AccessToken
import net.tardieu.roland.hciandroid.data.network.HCIAuthApiService
import net.tardieu.roland.hciandroid.data.network.RefreshToken
import retrofit2.Response
import java.net.SocketTimeoutException
import java.net.UnknownHostException

interface AccessTokenRepository {

    fun getRefreshedAccessToken(
        refreshToken: String,
    ): Flow<RepositoryResponse<AccessToken>>

}

class AccessTokenRepositoryImpl(
    private val hciAuthApiService: HCIAuthApiService
) : AccessTokenRepository {

    companion object {
        private const val TAG = "AccessTokenRepository"
    }

    override fun getRefreshedAccessToken(
        refreshToken: String
    ): Flow<RepositoryResponse<AccessToken>> = flow {
        emit(RepositoryResponse.Loading)
        var authApiServiceResponse: Response<AccessToken>? = null
        try {
            authApiServiceResponse = hciAuthApiService.refreshToken(RefreshToken(refreshToken))
            val result = if (authApiServiceResponse.isSuccessful) {
                RepositoryResponse.Success(authApiServiceResponse.body() ?: AccessToken())
            } else {
                // TODO("Define different error codes following failure cause")
                RepositoryResponse.Failure(
                    errorMessage = authApiServiceResponse.message(),
                    code = RepositoryResponse.ErrorCode.REFRESH_TOKEN_INVALID
                )
            }
            emit(result)
        } catch (e: SocketTimeoutException) {
            Log.e(TAG, e.toString())
            emit(RepositoryResponse.Failure("Socket timeout exception", RepositoryResponse.SOCKET_TIMEOUT_EXCEPTION))
        } catch (e: UnknownHostException) {
            Log.e(TAG, e.toString())
            emit(RepositoryResponse.Failure("Unknown host exception", RepositoryResponse.UNKNOWN_HOST_EXCEPTION))
        }

    }.flowOn(Dispatchers.IO)

}