package net.tardieu.roland.hciandroid.data.network

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.withTimeoutOrNull
import retrofit2.Response

sealed interface ApiResponse<out T> {
    data object Loading : ApiResponse<Nothing>

    data class Success<out T>(
        val data: T
    ) : ApiResponse<T>

    data class Failure(
        val errorMessage: String,
        val code: Int,
    ) : ApiResponse<Nothing>
}

fun<T> apiRequestFlow(call: suspend () -> Response<T>): Flow<ApiResponse<T>> = flow {
    //emit(ApiResponse.Loading)

    withTimeoutOrNull(20_000L) {
        val response = call()
        try {
            if (response.isSuccessful) {
                response.body()?.let { data ->
                    emit(ApiResponse.Success(data))
                }
            } else {
                response.errorBody()?.let { error ->
                    error.close()
                    // TODO("Implement error parsing to return acurate error information")
                    emit(ApiResponse.Failure("Erreur Api", 999))
                }
            }
        } catch (e: Exception) {
            emit(ApiResponse.Failure(e.message ?: e.toString(), 400))
        } ?: emit(ApiResponse.Failure("Timeout! Please try again", 408))
    }
}.flowOn(Dispatchers.IO)