package net.tardieu.roland.hciandroid.data.network

import kotlinx.serialization.Serializable

@Serializable
data class Credentials(
    val username: String,
    val password: String,
)

@Serializable
data class AccessAndRefreshToken(
    val access: String = "",
    val refresh: String = "",
)

@Serializable
data class AccessToken(
    val access: String = "",
)

@Serializable
data class RefreshToken(
    val refresh: String = "",
)