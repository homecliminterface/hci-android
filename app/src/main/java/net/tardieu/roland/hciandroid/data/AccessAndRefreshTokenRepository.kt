package net.tardieu.roland.hciandroid.data

import android.util.Log
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import net.tardieu.roland.hciandroid.data.network.AccessAndRefreshToken
import net.tardieu.roland.hciandroid.data.network.Credentials
import net.tardieu.roland.hciandroid.data.network.HCIAuthApiService
import retrofit2.Response
import java.net.SocketTimeoutException
import java.net.UnknownHostException

interface AccessAndRefreshTokenRepository {

    fun getAccessAndRefreshToken(
        username: String,
        password: String,
    ): Flow<RepositoryResponse<AccessAndRefreshToken>>

}

class AccessAndRefreshTokenRepositoryImpl(
    private val hciAuthApiService: HCIAuthApiService
) : AccessAndRefreshTokenRepository {

    companion object {
        private const val TAG = "AccessAndRefreshTokenRepository"
    }

    override fun getAccessAndRefreshToken(
        username: String,
        password: String,
    ): Flow<RepositoryResponse<AccessAndRefreshToken>> = flow {
        Log.d(TAG, "Function getAccessAndRefreshToken() called")
        emit(RepositoryResponse.Loading)
        Log.d(TAG, "Emitted 'Loading' response")
        val credentials = Credentials(
            username = username,
            password = password,
        )
        Log.d(TAG, "Calling hciAuthApiService.getTokenAndRefresh() with given credentials")
        Log.d(TAG, "username: $username; password: $password")
        var authApiServiceResponse: Response<AccessAndRefreshToken>? = null
        try {
            authApiServiceResponse = hciAuthApiService.getTokenAndRefresh(credentials)
            Log.d(TAG, "Response received from hciAuthApiService")
            val result = if (authApiServiceResponse.isSuccessful) {
                Log.d(TAG, "Call was successful")
                RepositoryResponse.Success(authApiServiceResponse.body() ?: AccessAndRefreshToken())
            } else {
                Log.d(TAG, "Call was unsuccessful")
                RepositoryResponse.Failure(
                    errorMessage = authApiServiceResponse.message(),
                    code = RepositoryResponse.ErrorCode.USERNAME_OR_PASSWORD_INVALID
                )
            }
            Log.d(TAG, "Emitting response ('Success' or 'Failure')")
            emit(result)
            when (result) {
                is RepositoryResponse.Success -> Log.d(TAG, "Emitted 'Success' response")
                is RepositoryResponse.Failure -> Log.d(TAG, "Emitted 'Failure' response")
                else -> Log.d(TAG, "Unexpected response status!!!")
            }
        } catch (e: SocketTimeoutException) {
            Log.e(TAG, e.toString())
            emit(RepositoryResponse.Failure("Socket timeout exception", RepositoryResponse.SOCKET_TIMEOUT_EXCEPTION))
        } catch (e: UnknownHostException) {
            Log.e(TAG, e.toString())
            emit(RepositoryResponse.Failure("Unknown host exception", RepositoryResponse.UNKNOWN_HOST_EXCEPTION))
        }
    }.flowOn(Dispatchers.IO)

}