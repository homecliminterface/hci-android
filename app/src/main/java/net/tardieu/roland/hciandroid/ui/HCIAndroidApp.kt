package net.tardieu.roland.hciandroid.ui

import androidx.annotation.StringRes
import androidx.compose.foundation.layout.padding
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material.icons.filled.Settings
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.currentBackStackEntryAsState
import androidx.navigation.compose.rememberNavController
import net.tardieu.roland.hciandroid.R
import net.tardieu.roland.hciandroid.ui.screens.settings.SettingsScreen
import net.tardieu.roland.hciandroid.ui.screens.valves.ValvesScreen

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun HCIAndroidAppBar(
    canNavigateBack: Boolean,
    navigateUp: () -> Unit,
    currentScreen: HCIAndroidScreen,
    onSettingsButtonClicked: () -> Unit,
    modifier: Modifier = Modifier,
) {
    TopAppBar(
        title = { Text(text = stringResource(id = currentScreen.title)) },
        colors = TopAppBarDefaults.mediumTopAppBarColors(
            containerColor = MaterialTheme.colorScheme.primaryContainer
        ),
        modifier = modifier,
        navigationIcon = {
            if (canNavigateBack) {
                IconButton(onClick = navigateUp) {
                    Icon(
                        imageVector = Icons.Filled.ArrowBack,
                        contentDescription = stringResource(id = R.string.back)
                    )
                }
            }
        },
        actions = {
            IconButton(onClick = onSettingsButtonClicked) {
                Icon(Icons.Filled.Settings, null)
            }
        }
    )
}

@Composable
fun HCIAndroidApp(
    navController: NavHostController = rememberNavController(),
) {
    val backStackEntry by navController.currentBackStackEntryAsState()

    Scaffold(
        topBar = {
            HCIAndroidAppBar(
                canNavigateBack = navController.previousBackStackEntry != null,
                navigateUp = { navController.navigateUp() },
                currentScreen = HCIAndroidScreen.valueOf(
                    backStackEntry?.destination?.route ?: HCIAndroidScreen.Valves.name
                ),
                onSettingsButtonClicked = {
                    navController.navigate(HCIAndroidScreen.Settings.name)
                }
            )
        }
    ) { innerPadding ->
        NavHost(
            navController = navController,
            startDestination = HCIAndroidScreen.Valves.name,
            modifier = Modifier
                .padding(innerPadding)
        ) {
            composable(route = HCIAndroidScreen.Valves.name) {
                ValvesScreen()
            }
            composable(route = HCIAndroidScreen.Settings.name) {
                SettingsScreen()
            }
        }
    }
}

enum class HCIAndroidScreen(@StringRes val title: Int) {
    Valves(R.string.screen_label_valves),
    Settings(R.string.screen_label_settings),
}