package net.tardieu.roland.hciandroid.data.network

import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.POST

interface HCIAuthApiService {
    @POST(value = "token")
    suspend fun getTokenAndRefresh(
        @Body credentials: Credentials
    ): Response<AccessAndRefreshToken>

    @POST(value = "token/refresh")
    suspend fun refreshToken(
        @Body refreshToken: RefreshToken
    ): Response<AccessToken>
}