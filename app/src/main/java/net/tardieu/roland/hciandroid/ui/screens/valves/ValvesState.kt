package net.tardieu.roland.hciandroid.ui.screens.valves

import net.tardieu.roland.hciandroid.data.Valve

data class ValvesState(
    val valves: List<ValveForUi> = listOf(),
)

data class ValveDetailsState(
    val valve: ValveForUi = ValveForUi(),
    val aggressiveTemperatureUpdate: Boolean = false,
)

/**
 * Data class used for ValvesScreen display of valves
 */
data class ValveForUi(
    val friendlyName: String = "",
    val heatRequired: Boolean = false,
    val heatingDemand: Int = 0,
    val wantedTemperature: Float = 0f,
    val measuredTemperature: Float = 0f,
    val scheduleName: String? = null
)

/**
 * Extension function to convert [Valve] to [ValveForUi].
 */
fun Valve.toValveForUi() = ValveForUi(
    friendlyName = friendlyName,
    heatRequired = heatRequired,
    heatingDemand = heatingDemand,
    wantedTemperature = wantedTemperature,
    measuredTemperature = measuredTemperature,
    scheduleName = null
)