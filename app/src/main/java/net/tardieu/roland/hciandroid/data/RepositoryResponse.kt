package net.tardieu.roland.hciandroid.data

sealed interface RepositoryResponse<out T> {

    data class Success<out T>(
        val data: T
    ) : RepositoryResponse<T>

    data class Failure(
        val errorMessage: String = "",
        val code: Int = 0
    ) : RepositoryResponse<Nothing>

    data object Loading : RepositoryResponse<Nothing>

    companion object ErrorCode {
        const val ACCESS_TOKEN_INVALID = 1901
        const val REFRESH_TOKEN_INVALID = 1902
        const val USERNAME_OR_PASSWORD_INVALID = 1903
        const val ACCESS_TOKEN_NOT_SET = 1904
        const val SOCKET_TIMEOUT_EXCEPTION = 1950
        const val UNKNOWN_HOST_EXCEPTION = 1951
    }
}