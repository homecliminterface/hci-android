package net.tardieu.roland.hciandroid.data.network

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import net.tardieu.roland.hciandroid.data.Valve

@Serializable
data class ApiValve(
    @SerialName(value = "ieee_address")
    val ieeeAddress: String,
    @SerialName(value = "friendly_name")
    val friendlyName: String,
    @SerialName(value = "heat_required")
    val heatRequired: Boolean,
    @SerialName(value = "measured_temperature")
    val measuredTemperature: Float,
    @SerialName(value = "wanted_temperature")
    val wantedTemperature: Float,
    @SerialName(value = "heating_demand")
    val heatingDemand: Int,
    @SerialName(value = "schedule_id")
    val scheduleId: Int,
    @SerialName(value = "schedule_name")
    val scheduleName: String,
)

fun ApiValve.toValve() = Valve(
    friendlyName = friendlyName,
    heatRequired = heatRequired,
    measuredTemperature = measuredTemperature,
    wantedTemperature = wantedTemperature,
    heatingDemand = heatingDemand,
    scheduleId = scheduleId
)
