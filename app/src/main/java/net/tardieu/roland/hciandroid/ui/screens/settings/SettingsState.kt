package net.tardieu.roland.hciandroid.ui.screens.settings

data class SettingsState(
    val username: String = "",
    val password: String = "",
    val showPassword: Boolean = false,
    val serverAddress: String = "",
)