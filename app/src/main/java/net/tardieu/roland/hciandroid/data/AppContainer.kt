package net.tardieu.roland.hciandroid.data

import android.content.Context
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.preferencesDataStore
import com.jakewharton.retrofit2.converter.kotlinx.serialization.asConverterFactory
import kotlinx.serialization.json.Json
import net.tardieu.roland.hciandroid.data.local.HCIAndroidDatabase
import net.tardieu.roland.hciandroid.data.network.AccessAndRefreshToken
import net.tardieu.roland.hciandroid.data.network.HCIAuthApiService
import net.tardieu.roland.hciandroid.data.network.HCIMainApiService
import okhttp3.MediaType.Companion.toMediaType
import retrofit2.Retrofit


/**
 * App container for dependency injection
 */
interface AppContainer {
    val valveRepository: ValveRepository
    val userPreferencesRepository: UserPreferencesRepository
    val accessAndRefreshTokenRepository: AccessAndRefreshTokenRepository
    val accessTokenRepository: AccessTokenRepository
}

/**
 * [AppContainer] implementation that provides instances for local repositories
 */
class AppDataContainer(private val context: Context): AppContainer {
    companion object {
        private const val USER_PREFERENCES_NAME = "user_preferences"
        private val Context.dataStore: DataStore<Preferences> by preferencesDataStore(
            name = USER_PREFERENCES_NAME
        )
        private const val BASE_URL = "https://hci.roland.tardieu.net"
    }

    private val retrofit: Retrofit by lazy {
        Retrofit.Builder()
            .addConverterFactory(Json.asConverterFactory("application/json".toMediaType()))
            .baseUrl(BASE_URL)
            .build()
    }
    private val retrofitMainService: HCIMainApiService by lazy {
        retrofit.create(HCIMainApiService::class.java)
    }
    private val retrofitAuthService: HCIAuthApiService by lazy {
        retrofit.create(HCIAuthApiService::class.java)
    }

    override val userPreferencesRepository: UserPreferencesRepository by lazy {
        UserPreferencesRepositoryImplementation(context.dataStore)
    }

    override val valveRepository: ValveRepository by lazy {
        ValveRepositoryImplementation(
            valveDao = HCIAndroidDatabase.getDatabase(context).valveDao(),
            hciMainApiService = retrofitMainService,
        )
    }

    override val accessAndRefreshTokenRepository: AccessAndRefreshTokenRepository by lazy {
        AccessAndRefreshTokenRepositoryImpl(
            hciAuthApiService = retrofitAuthService,
        )
    }

    override val accessTokenRepository: AccessTokenRepository by lazy {
        AccessTokenRepositoryImpl(
            hciAuthApiService = retrofitAuthService,
        )
    }
}