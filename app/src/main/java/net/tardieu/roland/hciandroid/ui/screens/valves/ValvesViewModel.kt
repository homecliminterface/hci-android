package net.tardieu.roland.hciandroid.ui.screens.valves

import android.text.format.DateFormat
import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.filter
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import net.tardieu.roland.hciandroid.data.AccessAndRefreshTokenRepository
import net.tardieu.roland.hciandroid.data.AccessTokenRepository
import net.tardieu.roland.hciandroid.data.RepositoryResponse
import net.tardieu.roland.hciandroid.data.UpdateStatus
import net.tardieu.roland.hciandroid.data.UserPreferencesRepository
import net.tardieu.roland.hciandroid.data.ValveRepository
import net.tardieu.roland.hciandroid.ui.ApiAuthState
import java.io.IOException
import java.util.Date

class ValvesViewModel(
    private val valveRepository: ValveRepository,
    private val accessAndRefreshTokenRepository: AccessAndRefreshTokenRepository,
    private val accessTokenRepository: AccessTokenRepository,
    private val userPreferencesRepository: UserPreferencesRepository,
) : ViewModel() {
    companion object {
        private const val TIMEOUT_MILLIS = 5_000L
        private const val TAG = "ValvesViewModel"
    }
    
    private val apiAuthState = MutableStateFlow(ApiAuthState())
    // TODO: Change method : The flow does not actualize itself because it is a single request on database
    // The repository should have a var List which can be changed an reemited in the flow. How to do this ?
    val valvesState: StateFlow<ValvesState> = valveRepository.allValvesStream
        .filter { it is RepositoryResponse.Success }
        .map { repositoryResponse ->
            when (repositoryResponse) {
                is RepositoryResponse.Success -> {
                    ValvesState(repositoryResponse.data.map { valve -> valve.toValveForUi() })
                }
                else -> throw IOException("Impossible to access valveDao (?)")
            }
        }
        .stateIn(
            scope = viewModelScope,
            started = SharingStarted.WhileSubscribed(TIMEOUT_MILLIS),
            initialValue = ValvesState()
        )
    val updateStatusState: StateFlow<UpdateStatus> = valveRepository.updateStatus
    val lastUpdateDateState: StateFlow<Date?> = valveRepository.updateStatus
        .filter { updateStatus -> updateStatus is UpdateStatus.Done }
        .map { updateStatus ->
            if (updateStatus is UpdateStatus.Done) updateStatus.date else null
        }
        .stateIn(
            scope = viewModelScope,
            started = SharingStarted.WhileSubscribed(TIMEOUT_MILLIS),
            initialValue = null
        )
    private val _valveDetailsState = MutableStateFlow(ValveDetailsState())
    val valveDetailsState: StateFlow<ValveDetailsState> = _valveDetailsState.asStateFlow()

    init {
        synchronizeApiAuthStateWithRepositoryFlow()
        // TODO : Find another way to do this
        viewModelScope.launch(Dispatchers.IO) {
            delay(1000)
            if (apiAuthState.value.accessToken.isBlank()
                && apiAuthState.value.username.isNotBlank()
                && apiAuthState.value.password.isNotBlank()) {
                setAccessAndRefreshTokenFromCredentials()
            }
        }
        subscribeToUpdateStatusStateAndReactToEmissions()
        subscribeToApiAuthStateAndReactToEmissions()
    }

    private fun synchronizeApiAuthStateWithRepositoryFlow() {
        viewModelScope.launch(Dispatchers.IO) {
            userPreferencesRepository.username
                .collect { username ->
                    apiAuthState.update { currentState ->
                        currentState.copy(username = username)
                    }
                    Log.d(TAG, "ApiAuthState username updated from flow: $username")
                }
        }
        viewModelScope.launch(Dispatchers.IO) {
            userPreferencesRepository.password
                .collect { password ->
                    apiAuthState.update {currentState ->
                        currentState.copy(password = password)
                    }
                    Log.d(TAG, "ApiAuthState password updated from flow: $password")
                }
        }
        viewModelScope.launch(Dispatchers.IO) {
            userPreferencesRepository.accessToken
                .collect { accessToken ->
                    apiAuthState.update {currentState ->
                        currentState.copy(accessToken = accessToken)
                    }
                    Log.d(TAG, "ApiAuthState access token updated from flow: $accessToken")
                }
        }
        viewModelScope.launch(Dispatchers.IO) {
            userPreferencesRepository.refreshToken
                .collect { refreshToken ->
                    apiAuthState.update {currentState ->
                        currentState.copy(refreshToken = refreshToken)
                    }
                    Log.d(TAG, "ApiAuthState refresh token updated from flow: $refreshToken")
                }
        }
    }

    private fun setAccessAndRefreshTokenFromCredentials() {
        viewModelScope.launch(Dispatchers.IO) {
            // TODO: Find another way to wait for username and password...
            delay(3000)
            Log.d(TAG, "Trying to set access and refresh token from credentials...")
            accessAndRefreshTokenRepository.getAccessAndRefreshToken(
                username = apiAuthState.value.username,
                password = apiAuthState.value.password,
            )
                .collect { repositoryResponse ->
                    when (repositoryResponse) {
                        is RepositoryResponse.Loading -> {
                            Log.d(TAG, "Token repository response is 'Loading'")
                        }
                        is RepositoryResponse.Success -> {
                            Log.d(TAG, "Token repository response is 'Success'")
                            val accessAndRefreshToken = repositoryResponse.data
                            userPreferencesRepository.saveAccessToken(accessAndRefreshToken.access)
                            userPreferencesRepository.saveRefreshToken(accessAndRefreshToken.refresh)
                            Log.d(TAG, "Access and refresh token updated from API service.")

                        }
                        is RepositoryResponse.Failure -> {
                            Log.e(TAG, "Token repository response is 'Failure'")
                            Log.e(TAG, "Error when trying to set access and refresh token from API service.")
                        }
                    }
                    // TODO: Unsubscribe when update is done ?
                }
        }
    }

    private fun refreshAccessTokenFromRefreshToken() {
        viewModelScope.launch(Dispatchers.IO) {
            Log.d(TAG, "Trying to refresh access token from refresh token...")
            accessTokenRepository.getRefreshedAccessToken(
                refreshToken = apiAuthState.value.refreshToken
            )
                .collect { repositoryResponse ->
                    when (repositoryResponse) {
                        is RepositoryResponse.Loading -> {}
                        is RepositoryResponse.Success -> {
                            val accessToken = repositoryResponse.data
                            userPreferencesRepository.saveAccessToken(accessToken.access)
                            Log.d(TAG, "Access token updated from API service")
                        }
                        is RepositoryResponse.Failure -> {
                            Log.e(TAG, "Error when trying to refresh access token from refresh token")
                        }
                    }
                }
        }
    }

    private fun updateValvesLocalDataFromApiService() {
        viewModelScope.launch(Dispatchers.IO) {
            // TODO : Remove when fixed
            valveRepository.updateValvesListFromApi()
        }
    }

    private fun subscribeToUpdateStatusStateAndReactToEmissions() {
        // TODO: Find a way to wait for token update before launching again valves update
        viewModelScope.launch(Dispatchers.IO) {
            updateStatusState.collect { updateStatus ->
                when (updateStatus) {
                    is UpdateStatus.AccessTokenNotPresent -> {
                        if (apiAuthState.value.accessToken.isBlank()) {
                            setAccessAndRefreshTokenFromCredentials()
                            valveRepository.setAccessToken(apiAuthState.value.accessToken)
                        }
                        valveRepository.setAccessToken(apiAuthState.value.accessToken)
                        valveRepository.updateValvesListFromApi()
                    }
                    is UpdateStatus.AccessTokenInvalid -> {
                        refreshAccessTokenFromRefreshToken()
                    }
                    else -> {}
                }
            }
        }
    }

    private fun subscribeToApiAuthStateAndReactToEmissions() {
        viewModelScope.launch(Dispatchers.IO) {
            delay(5000)
            apiAuthState.collect { apiAuth ->
                if (apiAuth.accessToken.isNotBlank()) {
                    valveRepository.setAccessToken(apiAuth.accessToken)
                    valveRepository.updateValvesListFromApi()
                }

            }
        }
    }

    fun refresh() = updateValvesLocalDataFromApiService()
}